import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import pylab as pl
import General_function_HOD as GFHOD
import sys
from scipy import interpolate
import time


#gaussian velocity dispersion function
def gaussv (y_3d, rr_3d, r_par_3d,xir_3d, fvel):
    
    #calculating 3d arrays of mu(r) and v12(r)
    mu_3d=y_3d/rr_3d
    vr_3d=np.interp(rr_3d, v12[:,0], fvel*v12[:,1])
    
    #velocity dispersion
    sigp_3d=np.interp(rr_3d, s12_p[:,0], s12_p[:,1])
    sigv_3d=np.interp(rr_3d, s12_v[:,0], s12_v[:,1])
    #combining velocity dispersion
    sigr_3d= np.power(mu_3d,2)*np.power(sigp_3d, 1)+((1-np.power(mu_3d,2))*np.power(sigv_3d, 1))
    sigr_3d=sigr_3d*fvel*fvel/np.power(1.0+xir_3d,2)

    #gaussian normalisation
    norm = np.power((2*np.pi*sigr_3d),(-0.5))
    
    f_g = norm*np.exp(np.power((r_par_3d)-y_3d-(mu_3d*vr_3d),2)/(-2*sigr_3d)) #gaussian
    return f_g, sigr_3d


def expv (y_3d, rr_3d, r_par_3d,xir_3d, fvel):
    
    #calculating 3d arrays of mu(r) and v12(r)
    mu_3d=y_3d/rr_3d
    vr_3d=np.interp(rr_3d, v12[:,0], fvel*v12[:,1])
    
    #velocity dispersion
    sigp_3d=np.interp(rr_3d, s12_p[:,0], s12_p[:,1])
    sigv_3d=np.interp(rr_3d, s12_v[:,0], s12_v[:,1])
    #combining velocity dispersion
    sigr_3d= np.power(mu_3d,2)*np.power(sigp_3d, 1)+((1-np.power(mu_3d,2))*np.power(sigv_3d, 1))
    sigr_3d=fvel*fvel*sigr_3d/np.power(1.0+xir_3d,2)

    #normalisation
    norm = np.power((2*sigr_3d),(-0.5))
    f_g = norm*np.exp(-np.sqrt(2)*np.sqrt(np.power(r_par_3d-y_3d-(mu_3d*vr_3d),2))/np.sqrt(sigr_3d)) #exponential
    
    
    return f_g, sigr_3d

#integration function
def integrate(s12_p, s12_v, v12, xi_r,mode, vmode, ya, yb, mua, mub, ny, nmu, fvel):
    
    #interval width for composite midpoint method
    hy = float(yb-ya)/ny
    hmu = float(mub-mua)/nmu
    
    #all the position axes
    y_ax = np.linspace((ya)+(hy/2), (yb)-(hy/2), ny)

    mu_s = np.linspace(mua+(hmu/2), mub-(hmu/2), nmu)
    
    
    #creating y axis over which the integral is performed, sampling-number of intervals
    y = y_ax
    #creating 3D arrays, corresponding to the given axes and y
    
    #creating an array of (real) r values that correspond to each point of r_L, r_II, y
    if(mode=='rppi'):
        rperp3d, y3d, rpar3d = np.meshgrid(r_perp,y,r_par)
        y3d = rpar3d-y3d
        rr_3d=np.sqrt(np.power(rperp3d,2)+np.power(y3d,2))
        rpar_3d=rpar3d
    elif(mode=='rmu'):
        s3d, y3d, mu_s3d = np.meshgrid(s,y,mu_s)
        rpar_3d=mu_s3d*s3d
        y3d = rpar_3d-y3d
        rr_3d=np.sqrt(np.power(y3d,2)-np.power(rpar_3d,2)+np.power(s3d,2))



    #xi_r function in 3d
    xir_3d=np.interp(rr_3d, xi_r[:,0], xi_r[:,1])


    #velocity dispersion function
    if vmode =="gauss":
        f_g, sigr_3d=gaussv(y3d,rr_3d, rpar_3d,xir_3d, fvel)
    
    elif vmode =="exp":
        f_g, sigr_3d=expv(y3d,rr_3d, rpar_3d,xir_3d, fvel)
    
    Intg_3d=(1+xir_3d)*f_g #the integrand

    
    return hy*np.sum(Intg_3d,axis=0), xir_3d , y_ax, mu_s  , sigr_3d, Intg_3d


#multipoles function for real space function, takes in real xi values, s axis, min mu value, max mu value, sampling of integration
def multipoles (xix, sax, a, b, n):
    
    h = float(b-a)/n
    mu = np.linspace(a+h/2, b-h/2, n)
    
    xi2d, mu2d = np.meshgrid(xix, mu)
    xi2d = xi2d.T
    
    mu_s_2d,s_2d = np.meshgrid(mu,sax)
    
    L_0=1
    L_2=(3*np.power(mu_s_2d,2)-1)*0.5
    L_4=(1./8.)*(35*np.power(mu_s_2d,4)-30*np.power(mu_s_2d,2)+3)
    
    xis_0 = ((2*0+1)/2.) * h*np.sum(xi2d*L_0,axis=1)
    xis_2 = ((2*2+1)/2.) * h*np.sum(xi2d*L_2, axis=1)
    xis_4 = ((2*4+1)/2.) * h*np.sum(xi2d*L_4, axis=1)
    
    return xis_0, xis_2, xis_4

#function to obtain redshift space multipoles
def multipoles2 (xi, sax, a, b, n, mus):
    
    
    hh = float(b-a)/n
    mu = np.linspace(a+hh/2, b-hh/2, n)
    
    mu_s_2d,s_2d = np.meshgrid(mu,sax)
    
    L_0=1
    L_2=(3*np.power(mu_s_2d,2)-1)*0.5
    L_4=(1./8.)*(35*np.power(mu_s_2d,4)-30*np.power(mu_s_2d,2)+3)
    
    xis_0 = ((2*0+1)/2.) * hh*np.sum(xi*L_0,axis=1)
    xis_2 = ((2*2+1)/2.) * hh*np.sum(xi*L_2, axis=1)
    xis_4 = ((2*4+1)/2.) * hh*np.sum(xi*L_4, axis=1)
    
    
    return xis_0, xis_2, xis_4

'''
SET UP
'''

if len(sys.argv)!=5: print "USAGE: coord, sampling, vmode,fmode", exit()



#opening data files and putting them in arrays
s12_p = np.loadtxt("/Users/agnesemenaite/Documents/SH/PeculiarVmodels-master/tmpdata/s12_p.txt") #velocity dispersion
s12_v = np.loadtxt("/Users/agnesemenaite/Documents/SH/PeculiarVmodels-master/tmpdata/s12_v.txt") #velocity dispersion
v12 = np.loadtxt("/Users/agnesemenaite/Documents/SH/PeculiarVmodels-master/tmpdata/v12.txt")  # mean velocities
xi_r = np.loadtxt("/Users/agnesemenaite/Documents/SH/PeculiarVmodels-master/tmpdata/xi_r.txt") #correlation function
xi_s = np.loadtxt("/Users/agnesemenaite/Documents/SH/PeculiarVmodels-master/tmpdata/xi_s.txt") #correlation function



#creating model function
position = np.linspace(1,100,150)
alfa = -0.008
A = 0.85/(np.power(10,alfa))
fr = A*np.power(position, alfa)
fr_ar = np.column_stack([position, fr])



#coordinates mode "rppi" or "smu"
coord = sys.argv[1]
samplingy = np.int(sys.argv[2])
#vmode exp or gauss
vmode = sys.argv[3]
fmode = sys.argv [4]

#velocity scaling value
fvel = 1.

#redshift space separation axes
r_perp = np.linspace(-150, 150,101)
r_par = np.linspace(-150, 150,110)
s = np.linspace (0,120,100)


#2d separation arrays
rper_2d, rpar_2d = np.meshgrid(r_perp, r_par)
rr2d = np.sqrt(np.power(rper_2d,2)+np.power(rpar_2d,2))

#mode that produces xi(s,mu)
if coord == "smu":
    
    #obtain xi in redshift space
    if fmode == "xi":
        
        #plotting real space xi from the file
        xir_2d=np.interp(rr2d, xi_r[:,0], xi_r[:,1])
        
        #checking for nans
        ind1=np.isnan(xir_2d)
        ind2=np.isinf(xir_2d)
        xir_2d[ind1]=1
        xir_2d[ind2]=1

        
        #plot 2d xi in real space
        pl.figure(figsize=(10,10))
        pl.pcolormesh(r_perp,r_par,np.log10(np.abs(np.power(rr2d,2)*xir_2d)))
        pl.colorbar()
        pl.contour(r_perp,r_par,np.log10(np.power(rr2d,2)*np.abs(xir_2d)),colors='k',lw=2)
        pl.axis('equal')
        pl.title("$xi_r$",size=30)
        pl.xlabel("$r_{perp}$", size=20)
        pl.ylabel("$r_{par}$", size=20)
        pl.show()
        
        #integration
        XI_S, XI, y_ax, mu_ax ,sig3d_r, xintg = integrate(s12_p, s12_v, v12, xi_r,'rmu', vmode, -200, 200, -1, 1, samplingy, 100, fvel)
        XI_S = XI_S-1
        
        #multipole expansion
        XIS0, XIS2, XIS4 = multipoles2 (XI_S, s, -1, 1, 100, mu_ax)
        
        #plot redshift space multipoles
        #USAGE: multipoles2 (2d array XI(s, mu_s), s, mu_min, mu_max, mu sampling)
        #NOTE: mu axis must match the one used in "integrate" above
        R2=np.power(s,2)
        pl.plot(s,R2*XIS0,'r-',label='XI0')
        pl.plot(s,R2*XIS2,'g-',label='XI2')
        pl.plot(s,R2*XIS4,'b-',label='XI4')
        
        
        #plot real space multipoles
        xi, r = xi_r[:,1], xi_r[:,0]
        a, b, c = multipoles(xi,r, -1,1,200)
        R = r*r
        plt.plot(r, R*a, 'r--')
        plt.plot(r, R*b, 'g--')
        plt.plot(r, R*c, 'b--')
        plt.title("XI multipoles")
        pl.show()
    
    #obtain model function in redshift space
    if fmode == "frmode":


        
        
        #integrate usage: integrate(s12_p, s12_v, v12, xi_r, MODE, Y min , Y max, MU min, MU max, Y sampling, MU sampling)
        frfunc = fr_ar
        FR_S, FR, Fy_ax, Fmu_ax , Fsig3d_r, frintg= integrate(s12_p, s12_v, v12, frfunc,'rmu', vmode, -80, 80, -1, 1, samplingy, 100, fvel)
            
        FR_S = FR_S-1
            
        #multipole calculation
        #USAGE: multipoles2 (2d array XI(s, mu_s), s, mu_min, mu_max, mu sampling)
        #NOTE: mu axis must match the one used in "integrate" above
            
        FRS0, FRS2, FRS4 = multipoles2 (FR_S, s, -1, 1, 100, Fmu_ax)
        FRR0, FRR2, FRR4 = multipoles (frfunc[:,1], frfunc[:,0], -1, 1, 100) #real space multipoles
            
            
        #plot multipoles
            
        pl.plot(s,FRS0,'r-',label='$f_s2$')
        pl.plot(frfunc[:,0],FRR0,'r--',label='$f 2$')
  
            
        pl.xlabel("r Mpc/h", fontsize=18.)
        pl.ylabel("$f_0$", fontsize=18.)
        pl.xscale('log')


        pl.plot(s,FRS2,'-',label='$f_s2$', c = 'g',lw=2)
        pl.plot(frfunc[:,0],FRR2,'--',label='$f 2$',c='green',lw=2)
        pl.plot(s,FRS4,'-',label='$f_s4$', c='b',lw=2)
        pl.plot(frfunc[:,0],FRR4,'--',label='$f 4$', c='blue',lw=2)

        pl.show()


    exit()

#mode that produces xi(r_perp, r_par)
elif coord=="rppi":
    
    #obtain xi in redshift space
    if fmode == "xi":
        
        XI_S, XI, y_ax , mu_ax, sig3d_r, xintg= integrate(s12_p, s12_v, v12, xi_r,'rppi', vmode, -80, 80, -1, 1, samplingy, 200, fvel)
        XI_S = XI_S.transpose()
        XI_S = XI_S-1
        
        #replacing nans
        indnan=np.isnan(XI_S)
        XI_S[indnan]=10
        
        #plotting xi multipoles in redshift space
        rrarr = np.linspace(0,120,100); muarr = np.linspace(0,1,200)
        Irmu_dicS=GFHOD.rmuweight_mapping(r_perp,r_par,rrarr,muarr,mode='lin')
        pl.figure(figsize=(10,10))
        
        xismu=GFHOD.Interpolate_xi((XI_S.T),Irmu_dicS)
        xi024=GFHOD.Xi_Legendre('jnk',rrarr,muarr,xismu,samp='rmu',write=0)
        
        r2=np.power(xi024[:,0],2)
        pl.plot(xi024[:,0],r2*xi024[:,1],'r-',label='xi0_s')
        pl.plot(xi024[:,0],r2*xi024[:,2],'g-',label='xi2_s')
        pl.plot(xi024[:,0],r2*xi024[:,3],'b-',label='xi4_s')
        pl.title("$xi_{s}, GFHOD$",size=30)
        pl.xlabel("$log_{10}(r)$ (Mpc/h)")
        
        XI_RR=XI[:,:,0] #real xi
        
        
        #plotting xi multipoles in real space
        
        XI_R = np.interp(rr2d,xi_r[:,0], xi_r[:,1])
        
        xirmu=GFHOD.Interpolate_xi(XI_R.transpose(),Irmu_dicS)
        XI024=GFHOD.Xi_Legendre('jnk',rrarr,muarr,xirmu,samp='rmu',write=0)
        
        R2=np.power(XI024[:,0],2)
        
        pl.plot(xi024[:,0],R2*XI024[:,1],'r--',label='XI0_R')
        pl.plot(xi024[:,0],R2*XI024[:,2],'b--',label='XI2_R')
        pl.plot(xi024[:,0],R2*XI024[:,3],'g--',label='XI4_R')
        pl.legend(fontsize=14)
        pl.title("$xi_r, GFHOD$",size=30)
        
        pl.show()
        
        
        #plotting 2d redshift space xi
        #pl.figure(figsize=(10,10))
        xis_plot=pl.pcolormesh(r_perp,r_par,np.log10(np.abs(XI_S)), vmin=np.nanmin(np.log10(np.abs(XI_S))))
        pl.contour(r_perp,r_par,np.log10(np.abs(XI_S)),colors='k',lw=2)
        pl.axis('equal')
        pl.colorbar(xis_plot)
        pl.title("$xi_s$", size=30)
        pl.xlabel("$r_p$", size=20)
        pl.ylabel("$r_{\parallel}$", size=20)
        pl.show()
        
        
    #obtain model function in redshift space
    if fmode == "frmode":
        

            
        frfunc = fr_ar
        #plotting real space fr
        fr_2d=np.interp(rr2d, frfunc[:,0], frfunc[:,1])
        
        #removing nans
        ind1=np.isnan(fr_2d)
        ind2=np.isinf(fr_2d)
        fr_2d[ind1]=1
        fr_2d[ind2]=1

        pl.figure(figsize=(10,10))
        pl.pcolormesh(r_perp,r_par,np.log10(np.abs(np.power(rr2d,0)*fr_2d)))
        pl.colorbar()
        pl.contour(r_perp,r_par,np.log10(np.power(rr2d,0)*np.abs(fr_2d)),colors='k',lw=2)
        pl.axis('equal')
        pl.title("$log_{10}f(r)$",size=30)
        pl.xlabel("$r_{\perp}$", size=20)
        pl.ylabel("$r_{\parallel}$", size=20)
        pl.show()
            
        FR_S, FR, Fy_ax, Fmu_ax , Fsig3d_r, frintg= integrate(s12_p, s12_v, v12, frfunc,'rppi', vmode, -80, 80, -1, 1, samplingy, 200, fvel)
        FR_S = FR_S-1
        FR_S = FR_S.transpose()
            
        
        #multipoles
        rrarr = np.linspace(0,120,100); muarr = np.linspace(0,1,200)
        Irmu_dicS=GFHOD.rmuweight_mapping(r_perp,r_par,rrarr,muarr,mode='lin')
        
        frsmu=GFHOD.Interpolate_xi((FR_S.T),Irmu_dicS)
        fr024=GFHOD.Xi_Legendre('jnk',rrarr,muarr,frsmu,samp='rmu',write=0)
        
        fr2d = np.interp(rr2d,frfunc[:,0], frfunc[:,1])
        frrmu=GFHOD.Interpolate_xi(fr2d.transpose(),Irmu_dicS)
        frr024=GFHOD.Xi_Legendre('jnk',rrarr,muarr,frrmu,samp='rmu',write=0)
        
        r22=np.power(fr024[:,0],2)
        r22 = 1.
        pl.plot(fr024[:,0],r22*fr024[:,1],'r--',label='fr0_s')
        pl.plot(frr024[:,0],r22*frr024[:,1],'r-',label='fr0_s')
        pl.legend()
        pl.title("$f_{r} (model function) multipoles$",size=30)
        pl.show()
            
        pl.plot(frr024[:,0],r22*frr024[:,2],'g-',label='fr2_r')
        pl.plot(frr024[:,0],r22*frr024[:,3],'b-',label='fr4_r')
        pl.plot(fr024[:,0],r22*fr024[:,2],'g--',label='fr2_s')
        pl.plot(fr024[:,0],r22*fr024[:,3],'b--',label='fr4_s')
        pl.legend()
        pl.title("$f_{r} (model function) multipoles$",size=30)

        pl.xscale('log')
        pl.show()
        
        #real space 2d model function
        pl.figure(figsize=(10,10))
        pl.pcolormesh(r_perp,r_par, FR[0,:,:].T)
        pl.colorbar()
        pl.contour(r_perp,r_par,FR[0,:,:].T,colors='k',lw=2)
        pl.axis('equal')
        pl.xlabel("$r_{perp}$", size=20)
        pl.ylabel("$r_{par}$", size=20)
        pl.show()
        
        
        #redshift space 2d model function
        pl.figure(figsize=(10,10))
        pl.pcolormesh(r_perp,r_par, np.log10(FR_S))
        pl.colorbar()
        pl.contour(r_perp,r_par,np.log10(FR_S),colors='k',lw=2)
        pl.axis('equal')
        #pl.title("$xi_r$",size=30)
        pl.title("$log_{10}f_s$",size=30)
        pl.xlabel("$r_{\perp}$", size=20)
        pl.ylabel("$r_{\parallel}$", size=20)
        pl.show()







